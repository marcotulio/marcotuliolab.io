module.exports = {
  title: 'Marco Túlio dev blog',
  description: 'Vue-powered static site generator running on GitLab Pages',
  base: '/',
  dest: 'public'
}
